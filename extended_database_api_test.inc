<?php
// $Id$

/**
 * @file
 * Unit test.
 */

/**
 * Executes an SQL query and fetch a result rows as an array
 * array db_select( string $query, [ mixed arg1 [, mixed arg2 ... ]])
 * Returns an array of fetched rows
 */
function extended_database_api_test() {
  $output = array('');
  
  // db_select
  $output[] = "------------------------------------------";
  $output[] = "Test db_select:";
  $output[] = "------------------------------------------";
  $query = "SELECT uid, name, mail FROM {users}";
  $output[] = "Query: $query";
  $rows = db_select($query);
  $output[] = 'Execution: db_select($query);';
  $output[] = "Result: ". _fetch_debug($rows);
  
  // db_select_row
  $output[] = "------------------------------------------";
  $output[] = "Test db_select_row:";
  $output[] = "------------------------------------------";
  $query = "SELECT uid, name, mail FROM {users} WHERE uid = %d";
  $output[] = "Query: $query";
  $row = db_select_row($query, 1);
  $output[] = 'Execution: db_select_row($query, 1);';
  $output[] = "Result: ". _fetch_debug($row);
  
  return implode('<br/>', $output);
}

function _fetch_debug($var) {
  return '<xmp>' . print_r($var, TRUE) . '</xmp>';
}