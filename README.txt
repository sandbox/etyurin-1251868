// $Id$

This module provides developer with a set of functions for more efficient data
extraction under drupal 6 core. Using this module, a developer can easily
extract keyed arrays, columns, single values and even trees as easy, as using
native drupal database api.

Project's mission is to add flexibility without sacrificing simplicity of using
database layer for developer. The original module doesn't clone or copy
functionality of drupal 7 database layer, which is more focused on interacting
with database, rather then data extraction.